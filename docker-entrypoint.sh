#!/usr/bin/env bash
KEYCLOAK_DIR=/keycloak/keycloak-17.0.1

echo "Environment variables:"
env
echo "\n\n"

$KEYCLOAK_DIR/bin/kc.sh start \
    --db-url $DB_URL \
    --db-username $DB_USER \
    --db-password $DB_PASSWORD  \
    --hostname=$KEYCLOAK_HOSTNAME \
    --https-port=443 \
    --proxy $KEYCLOAK_PROXY_MODE \
    --http-enabled=$KEYCLOAK_HTTP_ENABLED \
    --https-certificate-file=$TLS_CERT_FILE_PATH \
    --https-certificate-key-file=$TLS_KEY_FILE_PATH $KEYCLOAK_ARGS



FROM arm32v7/adoptopenjdk:11-jdk-hotspot-focal
#FROM openjdk:11

WORKDIR /keycloak

ADD https://github.com/keycloak/keycloak/releases/download/17.0.1/keycloak-17.0.1.tar.gz /keycloak
COPY ./docker-entrypoint.sh /

RUN chmod +x /docker-entrypoint.sh && \
    tar -xf keycloak-17.0.1.tar.gz && \
    ./keycloak-17.0.1/bin/kc.sh build --db postgres

ENV KEYCLOAK_EXTRA_ARGS=""
ENV PROXY_ADDRESS_FORWARDING="true"
ENV KEYCLOAK_HTTP_ENABLED="true"
ENV KEYCLOAK_PROXY_MODE="passthrough"

EXPOSE 8080
EXPOSE 443

ENTRYPOINT ["/docker-entrypoint.sh"]